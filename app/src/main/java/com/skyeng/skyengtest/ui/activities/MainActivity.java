package com.skyeng.skyengtest.ui.activities;

import android.content.Intent;
import android.os.Bundle;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.skyeng.skyengtest.R;
import com.skyeng.skyengtest.mvp.presenter.MainPresenter;
import com.skyeng.skyengtest.mvp.view.MainView;

import butterknife.OnClick;

public class MainActivity extends BaseToolbarActivity implements MainView {

    @InjectPresenter
    MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        presenter.checkToken();
    }

    @OnClick(R.id.btnLogout)
    public void logoutClick(){
        presenter.logout();
    }

    @Override
    public void gotoAuthorizeScreen() {
        Intent authIntent = new Intent(this, AuthorizeActivity.class);
        startActivity(authIntent);
        finish();
    }
}