package com.skyeng.skyengtest.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.skyeng.skyengtest.R;
import com.skyeng.skyengtest.mvp.presenter.EnterCodePresenter;
import com.skyeng.skyengtest.mvp.view.EnterCodeView;
import com.skyeng.skyengtest.ui.dialogs.ProgressDialogFragment;
import com.skyeng.skyengtest.utils.SimpleTextWatcher;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Александр on 03.04.2017.
 */

public class EnterCodeActivity extends BaseToolbarActivity implements EnterCodeView {

    @InjectPresenter
    EnterCodePresenter presenter;

    @BindView(R.id.tvEnterCodeHint)
    TextView tvEnterCodeHint;

    @BindView(R.id.etCode)
    EditText etCode;

    @BindView(R.id.btnEnter)
    Button btnEnter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_code);

        presenter.setSavedEmail();
        etCode.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {
                if(TextUtils.isEmpty(editable.toString())){
                    btnEnter.setEnabled(false);
                } else {
                    btnEnter.setEnabled(true);
                }
            }
        });
    }

    @OnClick(R.id.btnEnter)
    public void btnEnterClick(){
        String code = etCode.getText().toString();
        presenter.login(code);
    }

    @OnClick(R.id.btnResendCode)
    public void btnResendCodeClick(){
        presenter.resendCode();
    }

    @Override
    public void showProgressDialog(int msgResId) {
        DialogFragment dialog = ProgressDialogFragment.newInstance(msgResId);
        dialog.show(getSupportFragmentManager(), ProgressDialogFragment.TAG);
    }

    @Override
    public void hideProgressDialog() {
        ProgressDialogFragment prevDlg = (ProgressDialogFragment)getSupportFragmentManager().findFragmentByTag(ProgressDialogFragment.TAG);
        if (prevDlg != null) {
            prevDlg.dismiss();
        }
    }

    @Override
    public void gotoMainScreen() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void setEmail(String email) {
        tvEnterCodeHint.setText(getString(R.string.enter_code_code_email_hint, email));
    }
}