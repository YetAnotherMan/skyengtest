package com.skyeng.skyengtest.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.skyeng.skyengtest.R;
import com.skyeng.skyengtest.mvp.presenter.AuthorizePresenter;
import com.skyeng.skyengtest.mvp.view.AuthorizeView;
import com.skyeng.skyengtest.ui.dialogs.ProgressDialogFragment;
import com.skyeng.skyengtest.utils.SimpleTextWatcher;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Александр on 03.04.2017.
 */

public class AuthorizeActivity extends BaseToolbarActivity implements AuthorizeView {

    @InjectPresenter
    AuthorizePresenter presenter;

    @BindView(R.id.etEmail)
    EditText etEmail;

    @BindView(R.id.btnGetCode)
    Button btnGetCode;

    @BindView(R.id.etPassword)
    EditText etPassword;

    @BindView(R.id.btnUsualEnter)
    TextView btnUsualEnter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authorize);

        etEmail.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {
                if(TextUtils.isEmpty(editable.toString())){
                    btnGetCode.setEnabled(false);
                } else {
                    btnGetCode.setEnabled(true);
                }
            }
        });
    }

    @OnClick(R.id.btnGetCode)
    public void btnGetCodeClick(){
        String email = etEmail.getText().toString();
        String pass = etPassword.getText().toString();
        presenter.goToEnterCodeScreen(email, pass);
    }

    @OnClick(R.id.btnUsualEnter)
    public void btnUsualEnterClick(){
        presenter.changeUI();
    }

    @Override
    public void gotoEnterActivity() {
        Intent intent = new Intent(this, EnterCodeActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void gotoMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void showErrorMsg(int resId) {
        Snackbar.make(rootView, resId, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showDefaultProgressDialog() {
        DialogFragment dialog = ProgressDialogFragment.newInstance();
        dialog.show(getSupportFragmentManager(), ProgressDialogFragment.TAG);
    }

    @Override
    public void hideProgressDialog() {
        ProgressDialogFragment prevDlg = (ProgressDialogFragment)getSupportFragmentManager().findFragmentByTag(ProgressDialogFragment.TAG);
        if (prevDlg != null) {
            prevDlg.dismiss();
        }
    }

    @Override
    public void changeUIToUsualAuth() {
        etPassword.setVisibility(View.VISIBLE);
        btnGetCode.setText(R.string.enter_code_btn_enter);
        btnUsualEnter.setText(R.string.auth_btn_without_pass);
    }

    @Override
    public void changeUIToCodeAuth() {
        etPassword.setVisibility(View.GONE);
        btnGetCode.setText(R.string.auth_btn_get_code);
        btnUsualEnter.setText(R.string.auth_btn_usual_enter);
    }

    @Override
    public void showProgressDialog(int msgResId) {
        DialogFragment dialog = ProgressDialogFragment.newInstance(msgResId);
        dialog.show(getSupportFragmentManager(), ProgressDialogFragment.TAG);
    }
}