package com.skyeng.skyengtest.ui.activities;

import android.support.v7.widget.Toolbar;
import android.view.View;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.skyeng.skyengtest.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Александр on 03.04.2017.
 */

public class BaseToolbarActivity extends MvpAppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.rootView)
    protected View rootView;

    @Override
    public void setContentView(int layoutResID){
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
    }
}