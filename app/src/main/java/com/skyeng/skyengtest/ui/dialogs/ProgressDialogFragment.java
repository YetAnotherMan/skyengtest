package com.skyeng.skyengtest.ui.dialogs;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.skyeng.skyengtest.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Александр on 03.04.2017.
 */

public class ProgressDialogFragment extends DialogFragment {

    public static final String TAG = ProgressDialogFragment.class.getSimpleName();

    private static final String EXTRA_MSG = "etra_msg";

    @BindView(R.id.tvMessage)
    TextView tvMessage;

    public static ProgressDialogFragment newInstance(){
        return new ProgressDialogFragment();
    }

    public static ProgressDialogFragment newInstance(int resId){
        Bundle extras = new Bundle();
        extras.putInt(EXTRA_MSG, resId);

        ProgressDialogFragment dialog = new ProgressDialogFragment();
        dialog.setArguments(extras);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dialog_progress, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle bundle){
        super.onViewCreated(view, bundle);

        Bundle extras = getArguments();
        if( extras != null && extras.containsKey(EXTRA_MSG) ){
            int resId = extras.getInt(EXTRA_MSG);
            tvMessage.setText(resId);
        }
    }
}