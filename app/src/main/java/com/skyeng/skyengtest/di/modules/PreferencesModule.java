package com.skyeng.skyengtest.di.modules;

import android.content.Context;

import com.skyeng.skyengtest.utils.Preferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Александр on 04.04.2017.
 */
@Module
public class PreferencesModule {

    @Singleton
    @Provides
    Preferences providesPreferences(Context context){
        return new Preferences(context);
    }
}