package com.skyeng.skyengtest.di.modules;

import com.skyeng.skyengtest.api.SkyengAPIImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Александр on 04.04.2017.
 */
@Module
public class ApiModule {

    @Singleton
    @Provides
    SkyengAPIImpl providesAPI(){
        return new SkyengAPIImpl();
    }
}