package com.skyeng.skyengtest.di.components;

import android.content.Context;

import com.skyeng.skyengtest.api.SkyengAPIImpl;
import com.skyeng.skyengtest.di.modules.ApiModule;
import com.skyeng.skyengtest.di.modules.ContextModule;
import com.skyeng.skyengtest.di.modules.PreferencesModule;
import com.skyeng.skyengtest.mvp.presenter.AuthorizePresenter;
import com.skyeng.skyengtest.mvp.presenter.EnterCodePresenter;
import com.skyeng.skyengtest.mvp.presenter.MainPresenter;
import com.skyeng.skyengtest.utils.Preferences;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Александр on 04.04.2017.
 */

@Singleton
@Component(modules = {ContextModule.class, ApiModule.class, PreferencesModule.class})
public interface AppComponent {
    Context getContext();
    SkyengAPIImpl getApi();
    Preferences getPreferences();

    void inject(AuthorizePresenter presenter);
    void inject(EnterCodePresenter presenter);
    void inject(MainPresenter presenter);
}