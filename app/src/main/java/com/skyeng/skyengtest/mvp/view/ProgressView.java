package com.skyeng.skyengtest.mvp.view;

import com.arellomobile.mvp.MvpView;

/**
 * Created by Александр on 04.04.2017.
 */

public interface ProgressView extends MvpView {

    void showProgressDialog(int msgResId);
    void hideProgressDialog();

}
