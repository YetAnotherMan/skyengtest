package com.skyeng.skyengtest.mvp.view;

/**
 * Created by Александр on 03.04.2017.
 */

public interface AuthorizeView extends ProgressView {

    void gotoEnterActivity();
    void gotoMainActivity();
    void showErrorMsg(int resId);
    void changeUIToUsualAuth();
    void changeUIToCodeAuth();
    void showDefaultProgressDialog();

}