package com.skyeng.skyengtest.mvp.view;

/**
 * Created by Александр on 03.04.2017.
 */

public interface EnterCodeView extends ProgressView {

    void gotoMainScreen();
    void setEmail(String email);

}