package com.skyeng.skyengtest.mvp.presenter;

import android.os.Handler;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.skyeng.skyengtest.R;
import com.skyeng.skyengtest.api.SkyengAPIImpl;
import com.skyeng.skyengtest.app.TheApp;
import com.skyeng.skyengtest.mvp.view.EnterCodeView;
import com.skyeng.skyengtest.utils.Preferences;

import javax.inject.Inject;

/**
 * Created by Александр on 03.04.2017.
 */
@InjectViewState
public class EnterCodePresenter extends MvpPresenter<EnterCodeView> {

    private static final int TEST_DELAY = 2000;

    @Inject
    SkyengAPIImpl api;

    @Inject
    Preferences preferences;

    public EnterCodePresenter() {
        TheApp.getAppComponent().inject(this);
    }

    public void login(final String code){
        getViewState().showProgressDialog(R.string.auth_dlg_msg_enter);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getViewState().hideProgressDialog();
                api.login(code);
                preferences.saveToken(SkyengAPIImpl.JWT_TOKEN); //TODO for test only
                getViewState().gotoMainScreen();
            }
        }, TEST_DELAY);
    }

    public void resendCode(){
        api.sendCode();
        //TODO add UI logic
    }

    public void setSavedEmail(){
        String email = preferences.getEmail();
        getViewState().setEmail(email);
    }
}