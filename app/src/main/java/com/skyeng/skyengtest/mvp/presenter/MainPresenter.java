package com.skyeng.skyengtest.mvp.presenter;

import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.skyeng.skyengtest.app.TheApp;
import com.skyeng.skyengtest.mvp.view.MainView;
import com.skyeng.skyengtest.utils.Preferences;

import javax.inject.Inject;

/**
 * Created by Александр on 04.04.2017.
 */
@InjectViewState
public class MainPresenter extends MvpPresenter<MainView> {

    @Inject
    Preferences preferences;

    public MainPresenter() {
        TheApp.getAppComponent().inject(this);
    }

    public void logout(){
        preferences.eraseItems();
        getViewState().gotoAuthorizeScreen();
    }

    public void checkToken(){
        if( TextUtils.isEmpty(preferences.getToken() ) ){
            getViewState().gotoAuthorizeScreen();
        }
    }
}