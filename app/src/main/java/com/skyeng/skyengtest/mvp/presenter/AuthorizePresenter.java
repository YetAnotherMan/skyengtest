package com.skyeng.skyengtest.mvp.presenter;

import android.os.Handler;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.skyeng.skyengtest.R;
import com.skyeng.skyengtest.api.SkyengAPIImpl;
import com.skyeng.skyengtest.app.TheApp;
import com.skyeng.skyengtest.mvp.view.AuthorizeView;
import com.skyeng.skyengtest.utils.Preferences;

import javax.inject.Inject;

/**
 * Created by Александр on 03.04.2017.
 */
@InjectViewState
public class AuthorizePresenter extends MvpPresenter<AuthorizeView> {

    private static final int MODE_WITH_PASS = 2;
    private static final int MODE_WITHOUT_PASS = 1;

    private static final String CHECK_CHAR = "@";
    private static final int TEST_DELAY = 2000;

    //GOOD CREDENTIALS
    public static final String GOOD_EMAIL = "a@a.com";
    public static final String GOOD_PASS = "123";

    private int currentMode = MODE_WITHOUT_PASS;

    @Inject
    SkyengAPIImpl api;

    @Inject
    Preferences preferences;

    public AuthorizePresenter() {
        TheApp.getAppComponent().inject(this);
    }

    public void goToEnterCodeScreen(final String email, final String pass){
        if( currentMode == MODE_WITH_PASS ){
            //some simple validation
            if( !email.contains(CHECK_CHAR) ) {
                getViewState().showErrorMsg(R.string.err_email_incorrect);
                return;
            }

            getViewState().showProgressDialog(R.string.auth_dlg_msg_enter);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    getViewState().hideProgressDialog();

                    api.login(email, pass);

                    //TODO for test only, usual we have Rx observable from Retrofit2 adapter
                    if( email.equals(GOOD_EMAIL) && pass.equals(GOOD_PASS) ){
                        preferences.saveEmail(email);
                        preferences.saveToken(SkyengAPIImpl.JWT_TOKEN);
                        getViewState().gotoMainActivity();
                    } else {
                        getViewState().showErrorMsg(R.string.err_email_or_pass_incorrect);
                    }
                }
            }, TEST_DELAY);
        } else {
            //some simple validation
            if( !email.contains(CHECK_CHAR) ){
                getViewState().showErrorMsg(R.string.err_email_incorrect);
                return;
            }

            getViewState().showDefaultProgressDialog();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    getViewState().hideProgressDialog();
                    if( !email.contains(GOOD_EMAIL) ){
                        getViewState().showErrorMsg(R.string.err_email_incorrect);
                        return;
                    }

                    preferences.saveEmail(email);
                    getViewState().gotoEnterActivity();
                }
            }, TEST_DELAY);
        }
    }

    public void changeUI(){
        if( currentMode == MODE_WITHOUT_PASS ){
            getViewState().changeUIToUsualAuth();
            currentMode = MODE_WITH_PASS;
        } else {
            getViewState().changeUIToCodeAuth();
            currentMode = MODE_WITHOUT_PASS;
        }
    }
}