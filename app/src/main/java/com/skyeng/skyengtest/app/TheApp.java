package com.skyeng.skyengtest.app;

import android.app.Application;

import com.skyeng.skyengtest.di.components.AppComponent;
import com.skyeng.skyengtest.di.components.DaggerAppComponent;
import com.skyeng.skyengtest.di.modules.ContextModule;

/**
 * Created by Александр on 04.04.2017.
 */

public class TheApp extends Application {

    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder()
                .contextModule(new ContextModule(this))
                .build();

    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }
}
