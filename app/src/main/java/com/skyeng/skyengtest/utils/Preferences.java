package com.skyeng.skyengtest.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Александр on 03.04.2017.
 */

public class Preferences {

    private static final String PREFS_NAME = "skyeng_prefs";

    private static final String ITEM_JWT_TOKEN = "item_jwt_token";
    private static final String ITEM_EMAIL = "item_email";

    private SharedPreferences preferences;

    public Preferences(Context context) {
        preferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    public void saveToken(String token){
        preferences.edit().putString(ITEM_JWT_TOKEN, token).apply();
    }

    public String getToken(){
        return preferences.getString(ITEM_JWT_TOKEN, "");
    }

    public void saveEmail(String email){
        preferences.edit().putString(ITEM_EMAIL, email).apply();
    }

    public String getEmail(){
        return preferences.getString(ITEM_EMAIL, "");
    }

    public void eraseItems(){
        preferences.edit().clear().apply();
    }
}