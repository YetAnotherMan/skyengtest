package com.skyeng.skyengtest.api;

/**
 * Created by Александр on 03.04.2017.
 */
//must be interface for retrofit2
public interface SkyengAPI {

    void login(String email, String pass);
    void login(String code);
    void syncWords();
    void sendCode();

}